<?php
/**
 * Title: Header
 * Slug: taco/header
 */
?>
<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
	<div style="display: flex; flex-grow: 1;">
		<div class="navbar-brand">
		<a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="menu">
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
			<span aria-hidden="true"></span>
		</a>
		</div>

		<div id="menu" class="navbar-menu">
		<div class="navbar-start">
				<?php
					if (has_nav_menu('primary')) {
						wp_nav_menu(array(
							'container' 		=> '',
							'items_wrap' 		=> '%3$s',
							'theme_location' 	=> 'primary',
							'walker' 			=> new BulmaNavWalker
						));
					}
				?>
		</div>
		</div>
		<div class="secondary-menu">
			<?php if(get_theme_mod('taco_fb')) : ?>
			<a href="<?php echo get_theme_mod('taco_fb'); ?>" class="navbar-item">
				<span class="fa-stack fa-fb">
					<i class="fas fa-circle fa-stack-2x"></i>
					<i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
				</span>
			</a>
			<?php endif; ?>
			<?php if(get_theme_mod('taco_yt')) : ?>
			<a href="<?php echo get_theme_mod('taco_yt'); ?>" class="navbar-item">
				<span class="fa-stack fa-yt">
					<i class="fas fa-circle fa-stack-2x"></i>
					<i class="fab fa-youtube fa-stack-1x fa-inverse"></i>
				</span>
			</a>
			<?php endif; ?>
			<?php if(get_theme_mod('taco_ig')) : ?>
			<a href="<?php echo get_theme_mod('taco_ig'); ?>" class="navbar-item">
				<span class="fa-stack fa-ig">
					<i class="fas fa-circle fa-stack-2x"></i>
					<i class="fab fa-instagram fa-stack-1x fa-inverse"></i>
				</span>
			</a>
			<?php endif; ?>
			<a href="<?php echo is_user_logged_in() ? wp_logout_url(home_url()) : wp_login_url(home_url()); ?>" >
				<span class="button is-outlined">
					<?php echo is_user_logged_in() ? "Wyloguj się" : "Zaloguj się"; ?>
				</span>
			</a>
		</div>
	</div>
</nav>

<div class="section header">
	<div class="container">
		<div class="header-inner">
			<a href="/om-pttk/" class="logo-pttk">
				<img src="<?php echo get_template_directory_uri() . '/images/logo-pttk-white.png'; ?>" alt="Logo PTTK" />
			</a>
			<div style="display: flex; align-items: center;">
				<div class="motto">
					<p class="motto--content">
						W górach jest wszystko co kocham<br />
						Wszystkie wiersze są w bukach<br />
						Zawsze kiedy tam wracam<br />
						Biorą mnie klony za wnuka
					</p>
					<p class="motto--author">
						Jerzy Harasymowicz &bdquo;W górach&rdquo;
					</p>
				</div>
				<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'title' ) ); ?> &mdash; <?php echo esc_attr( get_bloginfo( 'description' ) ); ?>' rel='home' class="blog-logo">
					<img src='<?php echo esc_url( get_theme_mod( 'taco_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'title' ) ); ?>' />
				</a>
			</div>
		</div>
	</div>
</div>