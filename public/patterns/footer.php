<?php
/**
 * Title: Footer
 * Slug: taco/footer
 */
?>
<?php if (is_active_sidebar('bottom-sidebar')) : ?>
	<div class="section" style="padding-top: 0;">
		<div class="container" style="padding-top: 2.5rem; border-top: 1px solid #eee;">
			<?php dynamic_sidebar('bottom-sidebar'); ?>
		</div>
	</div>
<?php endif; ?>
<footer class="footer">
	<div class="container">
		<div class="credits">
			<span>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></span>
			<a class="tothetop"><?php _e('Up', 'taco'); ?> &uarr;</a>
		</div>
	</div>
</footer>

<!-- SVG used to style Instagram icon in the navbar. -->
<svg style="width:0;height:0;position:absolute;" aria-hidden="true" focusable="false">
       <radialGradient id="brand-gradient" cx="30%" cy="107%" r="127%">
    <stop offset="0%" stop-color="#fdf497" />
    <stop offset="5%" stop-color="#fdf497" />
    <stop offset="45%" stop-color="#fd5949" />
    <stop offset="60%" stop-color="#d6249f" />
    <stop offset="90%" stop-color="#285AEB" />
  </radialGradient>
</svg>