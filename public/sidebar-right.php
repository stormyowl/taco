<?php if (is_active_sidebar('right-sidebar')) : ?>
	<div id="sidebar" role="complementary">
		<?php dynamic_sidebar('right-sidebar'); ?>
	</div>
<?php endif; ?>
