<?php
require_once('classes/BulmaNavWalker.php');

if (!function_exists('taco_setup')) {
	function taco_setup() {
		// Automatic feed
		add_theme_support('automatic-feed-links');

		// Title tag
		add_theme_support('title-tag');

		// Set content width
		global $content_width;
		if (!isset($content_width)) $content_width = 676;


		register_nav_menu('primary', __('Primary Menu', 'taco'));
		load_child_theme_textdomain('taco', get_template_directory() . '/languages');
	}
  add_action('after_setup_theme', 'taco_setup');
}


/* ---------------------------------------------------------------------------------------------
   ENQUEUE SCRIPTS
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_load_javascript_files')) {

	function taco_load_javascript_files() {
		if (!is_admin()) {
			wp_enqueue_script(
				'taco',
				get_template_directory_uri('taco') . '/js/global.js',
				array(),
				wp_get_theme('taco')->get('Version'),
				true
			);
		}
	}
	add_action('wp_enqueue_scripts', 'taco_load_javascript_files');

}


/* ---------------------------------------------------------------------------------------------
   ENQUEUE STYLES
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_load_style')) {
	function taco_load_style() {
		if (!is_admin()) {
			wp_enqueue_style('lity-preload', '/wp-content/plugins/gdpr-cookie-compliance/dist/styles/lity.css', array(), null);

			$dependencies = array();

			wp_enqueue_style('taco_style', get_template_directory_uri() . '/style.css', $dependencies, wp_get_theme('taco')->get('Version'));
		}
	}
	add_action('wp_print_styles', 'taco_load_style');

}


/* ---------------------------------------------------------------------------------------------
   ADD WIDGET AREAS
   --------------------------------------------------------------------------------------------- */

if (!function_exists('taco_sidebar_registration')) {

	function taco_sidebar_registration() {
		register_sidebar(array(
			'name' => 'Right sidebar',
			'id' => 'right-sidebar',
			'description'	=> 'Widgets in this area will be shown in the right sidebar.',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
			'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
			'after_widget' => '</div><div class="clear"></div></div>'
		));
		register_sidebar(array(
			'name' => 'Bottom',
			'id' => 'bottom-sidebar',
			'description'	=> 'Widgets in this area will be shown in the bottom.',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
			'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
			'after_widget' => '</div></div>'
		));

	}
	add_action('widgets_init', 'taco_sidebar_registration');

}


/* ---------------------------------------------------------------------------------------------
   ADD CLASSES TO PAGINATION LINKS
   --------------------------------------------------------------------------------------------- */

if (!function_exists('taco_next_posts_link_class')) {

	function taco_next_posts_link_class() {
		return 'class="post-nav-older"';
	}
	add_filter('next_posts_link_attributes', 'taco_next_posts_link_class');

}

if (!function_exists('taco_prev_posts_link_class')) {

	function taco_prev_posts_link_class() {
		return 'class="post-nav-newer"';
	}
	add_filter('previous_posts_link_attributes', 'taco_prev_posts_link_class');

}


/* ---------------------------------------------------------------------------------------------
   BODY CLASSES
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_body_classes')) {
	function taco_body_classes($classes) {
		$classes[] = 'has-navbar-fixed-top';

		return $classes;
	}
	add_action('body_class', 'taco_body_classes');

}


/* ---------------------------------------------------------------------------------------------
   CUSTOM MORE LINK TEXT
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_custom_more_link')) {

	function taco_custom_more_link($more_link, $more_link_text) {
		return str_replace('more-link', 'more-link button', str_replace($more_link_text, __('Continue reading', 'taco'), $more_link));
	}
	add_filter('the_content_more_link', 'taco_custom_more_link', 10, 2);

}


/* ---------------------------------------------------------------------------------------------
   STYLE THE ADMIN AREA
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_admin_css')) {

	function taco_admin_css() { ?>
		<style type="text/css">
			#postimagediv #set-post-thumbnail img {
				max-width: 100%;
				height: auto;
			}
		</style>
		<?php
	}
	add_action('admin_head', 'taco_admin_css');

}


/* ---------------------------------------------------------------------------------------------
   CUSTOMIZER SETTINGS
   --------------------------------------------------------------------------------------------- */

add_action('customize_register', 'taco_customize');
function taco_customize($wp_customize) {
	$wp_customize->add_section(
		'taco_logo_section' ,
		array(
			'title'       => __('Logo', 'taco'),
			'priority'    => 40,
			'description' => __('Upload a logo to replace the default site name and description in the header','taco'),
		)
	);

	// Add logo setting and sanitize it
	$wp_customize->add_setting('taco_logo', array('sanitize_callback' => 'esc_url_raw'));

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'taco_logo',
			array(
				'label'    => __('Logo', 'taco'),
				'section'  => 'taco_logo_section',
				'settings' => 'taco_logo',
			)
		)
	);

	$wp_customize->get_setting('blogname')->transport = 'postMessage';
	$wp_customize->get_setting('blogdescription')->transport = 'postMessage';

	function sanitize_social_media_url($input) {
    return strip_tags(esc_url_raw($input, array('https')));
	}

	$wp_customize->add_section(
		'taco_social_media_section' ,
		array(
			'title'       => __('Social media', 'taco'),
			'priority'    => 50,
			'description' => __('Fill in links to the social media portals','taco'),
		)
	);

	$wp_customize->add_setting('taco_fb', array('sanitize_callback' => 'sanitize_social_media_url'));

	$wp_customize->add_control(
		'taco_fb',
		array(
			'name' => 'facebook',
			'label' => __('Facebook profile URL', 'taco'),
			'type' => 'url',
			'section' => 'taco_social_media_section',
			'input_attrs' => array(
		    'placeholder' => __('Your Facebook profile URL'),
		  ),
		)
	);

	$wp_customize->add_setting('taco_yt', array('sanitize_callback' => 'sanitize_social_media_url'));

	$wp_customize->add_control(
		'taco_yt',
		array(
			'name' => 'youtube',
			'label' => __('YouTube channel URL', 'taco'),
			'type' => 'url',
			'section' => 'taco_social_media_section',
			'input_attrs' => array(
		    'placeholder' => __('Your YouTube channel URL'),
		  ),
		)
	);

	$wp_customize->add_setting('taco_ig', array('sanitize_callback' => 'sanitize_social_media_url'));

	$wp_customize->add_control(
		'taco_ig',
		array(
			'name' => 'instagram',
			'label' => __('Instagram profile URL', 'taco'),
			'type' => 'url',
			'section' => 'taco_social_media_section',
			'input_attrs' => array(
		    'placeholder' => __('Your Instagram profile URL'),
		  ),
		)
	);
}


/*
 *
 * GUTENBERG FUNCTIONS
 *
 */


/* ---------------------------------------------------------------------------------------------
   SPECIFY GUTENBERG SUPPORT
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_add_gutenberg_features')) {

	function taco_add_gutenberg_features() {

		/* Gutenberg Feature Opt-Ins --------------------------------------- */

		add_theme_support('align-wide');
	}
	add_action('after_setup_theme', 'taco_add_gutenberg_features');

}


/* ---------------------------------------------------------------------------------------------
   GUTENBERG EDITOR STYLES
   --------------------------------------------------------------------------------------------- */


if (!function_exists('taco_block_editor_styles')) {

	function taco_block_editor_styles() {

		$dependencies = array();

		// Enqueue the editor styles
		wp_enqueue_style('taco-block-editor-styles', get_theme_file_uri('/taco-gutenberg-editor-style.css'), $dependencies, '1.0', 'all');
	}
	add_action('enqueue_block_editor_assets', 'taco_block_editor_styles');

}

function defer_script_loading($tag, $handle, $src) {
	$defer_scripts = array('lbwps-lib', 'lbwps-ui-default', 'lbwps-frontend');
	if (in_array($handle, $defer_scripts)) {
		if (false === stripos($tag, 'async')) {
			$tag = str_replace(' src', ' async="async" src', $tag);
		}

		if (false === stripos($tag, 'defer')) {
			$tag = str_replace('<script ', '<script defer ', $tag);
		}
	}

	return $tag;
}
add_filter('script_loader_tag', 'defer_script_loading', 10, 3);

function defer_style_loading($html, $handle, $href, $media) {
	$defer_styles = array('lity-preload', 'photoswipe-skin', 'lbwps-lib', 'moove_gdpr_frontend', 'simcal-default-calendar-list');
	if(in_array($handle, $defer_styles)) {
		$html = str_replace(
			'href',
			'as="style" onload="this.onload=null;this.rel=\'stylesheet\'" href',
			str_replace('stylesheet', 'preload', $html)
		);
	}

	return $html;
}
add_filter('style_loader_tag', 'defer_style_loading', 10, 4);

function conditionaly_deregister_styles() {
	if(!is_page('kursy-przewodnickie')) {
		wp_dequeue_style('iheg-image-hover');
	}
}
add_action('wp_enqueue_scripts', 'conditionaly_deregister_styles', 11);

function inline_header_styles() {
	$mobile_header_url = get_template_directory_uri() . '/images/header-768x300.jpg';
	$tablet_header_url = get_template_directory_uri() . '/images/header-1024x400.jpg';
	$desktop_header_url = get_template_directory_uri() . '/images/header-1216x400.jpg';
	$widescreen_header_url = get_template_directory_uri() . '/images/header-1408x400.jpg';
	$fullhd_header_url = get_template_directory_uri() . '/images/header-1920x400.jpg';
    echo <<<EOT
	<style>
		@media screen and (max-width: 768px) {
			.header {
				background-image: url($mobile_header_url);
			}
		}
		@media screen and (min-width: 769px) {
			.header {
				background-image: url($tablet_header_url);
			}
		}
		@media screen and (min-width: 1024px) {
			.header {
				background-image: url($desktop_header_url);
			}
		}
		@media screen and (min-width: 1216px) {
			.header {
				background-image: url($widescreen_header_url);
			}
		}
		@media screen and (min-width: 1408px) {
			.header {
				background-image: url($fullhd_header_url);
			}
		}
	</style>
	EOT;
}
add_action('wp_head', 'inline_header_styles', 100);

?>
