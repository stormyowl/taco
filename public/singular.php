<?php get_header(); ?>

			<div>
				<?php if (have_posts()):
					while (have_posts()):
						the_post();
				?>
				<div class="posts">
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="post-header">
							<?php if (get_the_title()) : ?>
								<?php if (is_single()) : ?>
									<h1 class="post-title">
										<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
									</h1>
								<?php else : ?>
									<h1 class="post-title"><?php the_title(); ?></h1>
								<?php endif; ?>
							<?php endif; ?>

							<?php if (is_single()) : ?>
								<div class="post-meta">
									<span class="post-date"><a href="<?php the_permalink(); ?>"><i class="far fa-calendar-alt"></i> <?php the_time(get_option('date_format')); ?></a></span>
									<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="post-author"><i class="fas fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></a>
									<?php
										if (comments_open()) {
											echo '<a href="' . get_comments_link() . '"><i class="fas fa-comments"></i> ' . get_comments_number() . '</a>';
										}
									?>
									<?php if ( current_user_can( 'manage_options' ) ) : ?>
										<?php edit_post_link('<i class="fas fa-pen-nib"></i> ' . __('Edit', 'taco')); ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div><!-- .post-header -->

						<div class="post-content">
							<?php the_content(); ?>
							<?php wp_link_pages(); ?>
						</div><!-- .post-content -->
						<?php if (is_single()) : ?>
							<div class="post-meta-bottom">

								<div class="post-terms">
									<?php
										echo join('', array_map(
											function($category) {
												return '<a href="' . esc_url(get_category_link($category->term_id)) . '" class="tag"><span><i class="fas fa-tag"></i> ' . esc_html($category->name) . '</span></a>';
											},
											get_the_category()
										));
										if (has_tag()) {
											echo join('', array_map(
												function($category) {
													return '<a href="' . esc_url(get_tag_link($category->term_id)) . '" class="tag"><span><i class="fas fa-hashtag"></i> ' . esc_html($category->name) . '</span></a>';
												},
												get_the_tags()
											));
										}
									?>
								</div>

								<?php
									$prev_post = get_previous_post();
									$next_post = get_next_post();
									if ($prev_post || $next_post):
								?>
									<div class="post-nav">
										<?php if ($prev_post) : ?>
											<a class="post-nav-older" href="<?php echo get_permalink($prev_post->ID); ?>">
												<h5><?php _e( 'Previous post', 'taco' ); ?></h5>
												<?php echo get_the_title( $prev_post->ID ); ?>
											</a>
										<?php endif; ?>
										<?php if ($next_post) : ?>
											<a class="post-nav-newer" href="<?php echo get_permalink($next_post->ID); ?>">
												<h5><?php _e( 'Next post', 'taco' ); ?></h5>
												<?php echo get_the_title( $next_post->ID ); ?>
											</a>
										<?php endif; ?>
									</div>
								<?php endif; ?>

							</div>
						<?php
							endif;
							endwhile;
							endif;
						?>
					</div><!-- .post -->
				</div><!-- .posts -->
			</div>
			<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
