<div id="post-<?php the_ID(); ?>" <?php post_class('post-preview'); ?>>

	<div class="post-header">
		<?php if (has_post_thumbnail()) : ?>
			<div class="featured-media">
				<?php if (is_sticky()) : ?><span class="sticky-post"><?php _e('Sticky post', 'taco'); ?></span><?php endif; ?>
				<a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php
					the_post_thumbnail('mobile');
					$image_caption = get_post(get_post_thumbnail_id())->post_excerpt;
					if ($image_caption) : ?>
						<div class="media-caption-container">
							<p class="media-caption"><?php echo $image_caption; ?></p>
						</div>
					<?php endif; ?>
				</a>
			</div>
		<?php endif; ?>

		<h2 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
		<div class="post-meta">
			<span class="post-date"><a href="<?php the_permalink(); ?>"><i class="far fa-calendar-alt"></i> <?php the_time(get_option('date_format')); ?></a></span>
			<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="post-autho"><i class="fas fa-user"></i> <?php echo get_the_author_meta('display_name'); ?></a>
			<?php
				if (comments_open()) {
					echo '<a href="' . get_comments_link() . '"><i class="fas fa-comments"></i> ' . get_comments_number() . '</a>';
				}
			?>
			<?php if (is_sticky() && !has_post_thumbnail()) : ?>
				<span><i class="fas fa-thumbtack"></i> <?php _e('Sticky', 'taco'); ?></span>
			<?php endif; ?>
			<?php edit_post_link('<i class="fas fa-pen-nib"></i> ' . __('Edit', 'taco')); ?>
		</div>
	</div>

	<?php if (get_the_content()) : ?>
		<div class="post-content">
			<?php the_content(); ?>
			<?php wp_link_pages(); ?>
		</div>
	<?php endif; ?>
	<div class="post-meta-bottom">
		<div class="post-terms">
			<?php
				echo join('', array_map(
					function($category) {
						return '<a href="' . esc_url(get_category_link($category->term_id)) . '" class="tag"><span><i class="fas fa-tag"></i> ' . esc_html($category->name) . '</span></a>';
					},
					get_the_category()
				));
				if (has_tag()) {
					echo join('', array_map(
						function($category) {
							return '<a href="' . esc_url(get_tag_link($category->term_id)) . '" class="tag"><span><i class="fas fa-hashtag"></i> ' . esc_html($category->name) . '</span></a>';
						},
						get_the_tags()
					));
				}
			?>
		</div>
	</div>
</div><!-- .post -->
