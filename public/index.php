<?php get_header(); ?>

			<div>
				<div class="posts">
					<?php
						$archive_title = '';
						if ( is_archive() ) {
							$archive_title = get_the_archive_title();
						} elseif ( is_search() ) {
							$archive_title = sprintf( _x( 'Search results: "%s"', 'Variable: search query text', 'taco' ), get_search_query() );
						}

						if ($archive_title) :
					?>
					<div class="page-title">
						<h4><?php echo $archive_title; ?></h4>
					</div>
					<?php endif; ?>

					<?php
						if (have_posts()):
							while (have_posts()):
								the_post();
								get_template_part('content');
							endwhile;
						elseif (is_search()):
					?>

					<div class="post">
						<div>
							<div class="post-content">
								<p><?php _e( 'No results. Try again, would you kindly?', 'taco' ); ?></p>
								<?php get_search_form(); ?>
							</div>
						</div>
					</div>
					<?php endif; ?>
				</div>

				<?php if ($wp_query->max_num_pages > 1) : ?>
					<nav class="pagination is-rounded is-centered archive-nav" role="navigation" aria-label="pagination">
						<?php
							$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
              echo str_replace(
								'post-nav-newer',
								'pagination-previous',
								get_previous_posts_link('<i class="fas fa-chevron-left"></i>')
							);
							echo str_replace(
								'post-nav-older',
								'pagination-next',
								get_next_posts_link('<i class="fas fa-chevron-right"></i>')
							);
							echo '<ul class="pagination-list">';
							echo str_replace(
								'page-numbers',
								'pagination-link',
								str_replace(
									'page-numbers dots',
									'pagination-ellipsis',
									str_replace(
										'page-numbers current',
										'pagination-link is-current',
										paginate_links(array(
											'current' => max(1, get_query_var('paged')),
											'total' => $wp_query->max_num_pages,
											'prev_next' => False
										))
									)
								)
							);
							echo '</ul>';
						?>
					</nav>
				<?php endif; ?>
			</div>

			<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
