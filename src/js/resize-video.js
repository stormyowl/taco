document.addEventListener('DOMContentLoaded', function() {
  function resizeVideo() {
    const selector = '.post iframe, .post object, .post video, .widget-content iframe, .widget-content object, .widget-content iframe';
		document.querySelectorAll(selector).forEach(function(video) {
			const targetWidth = video.parentElement.clientWidth

			if (!video.getAttribute('data-origwidth')) {
				video.setAttribute('data-origwidth', video.getAttribute('width'))
				video.setAttribute('data-origheight', video.getAttribute('height'))
			}

			const ratio = targetWidth / video.getAttribute('data-origwidth')

			video.style.width = `${targetWidth}px`
			video.style.height = `${video.getAttribute('data-origheight') * ratio}px`
		})
  }

	resizeVideo()

	window.onresize = resizeVideo
})
