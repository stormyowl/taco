document.addEventListener('DOMContentLoaded', function(event) {
  const burger = document.querySelector('.burger');
  const nav = document.querySelector(`#${burger.dataset.target}`);

  burger.addEventListener('click', function() {
    burger.classList.toggle('is-active');
    nav.classList.toggle('is-active');
  });

  const navbarItemsWithDropdown = document.querySelectorAll('.has-dropdown');
  navbarItemsWithDropdown.forEach(function(navbarItem) {
    navbarItem.addEventListener('click', function(event) {
      event.stopPropagation()
      const itemDropdown = event.target.parentNode.lastElementChild;
      const openedBefore = itemDropdown.classList.contains('is-expanded')

      function foo(node) {
        if(node) {
          if(node.classList.contains('navbar-dropdown')) {
            node.classList.add('is-expanded')
          }
          return foo(node.parentElement)
        }
      }
      if(!openedBefore) {
        document.querySelectorAll('.navbar-dropdown').forEach(function(dropdown) {
          dropdown.classList.remove('is-expanded')
        })
        foo(itemDropdown)
      } else {
        itemDropdown.classList.remove('is-expanded')
      }
    })
  })

  const navbarLinks = document.querySelectorAll('a.navbar-item:not(.navbar-link)')
  navbarLinks.forEach(function(navbarLink) {
    navbarLink.addEventListener('click', function(event) {
      document.getElementById('menu').classList.remove('is-active')
    })
  })
});
