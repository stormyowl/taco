import isTouchDevice from 'is-touch-device'

import './scss/wordpress.scss'
import './js/navbar.js'
import './js/to-the-top.js'
import './js/resize-video.js'
import './scss/main.scss'

document.addEventListener('DOMContentLoaded', function() {
  if(isTouchDevice()) {
    document.querySelectorAll('.eihe-caption').forEach(el => el.style.opacity = 0.9)
  }
})
