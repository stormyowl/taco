const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: {
    global: './src/global.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'js/[name].js',
    chunkFilename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(scss|sass|css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader' },
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(bmp|gif|jpe?g|png)$/,
        loader: require.resolve('file-loader'),
        options: {
          name: 'images/[name].[ext]',
        },
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: ({ chunk }) => chunk.name === 'global' ? 'style.css' : `${chunk.name}.css`,
      chunkFilename: '[name].css',
    }),
    new CleanWebpackPlugin(),
  ],
  optimization: {
    minimize: true,
    minimizer: [new CssMinimizerPlugin(), '...'],
    splitChunks: {
        cacheGroups: {
          commons: {
            name: 'commons',
            test: /\.(css|js)$/,
            chunks: 'all',
            minChunks: 2,
            enforce: true
          }
        }
      }
  },
};
